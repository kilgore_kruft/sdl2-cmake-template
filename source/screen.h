#ifndef CONSTANTS_H
#define CONSTANTS_H

/* The screen attributes */
extern int scr_w;
extern int scr_h;
extern int scr_s;

/* Constant screen attributes */
extern const int SCREEN_FPS;
extern const int SCREEN_TPF;

/* -- Ratio Adjustment -- */
int ratio_adj(int a, int b = scr_w, double r = 2);

/* -- Display Margin -- */
void dispMargin(int y, int sections = 25, int width = -1, int height = -1);

/* -- Get Mouse Coordinates -- */
void getMouseCoords(int &x, int &y);

#endif
