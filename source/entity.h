#ifndef ENTITY_H
#define ENTITY_H

/* Using SDL2 */
#include <SDL2/SDL.h>

/* Accessing coordinate superclass */
#include "coords.h"

typedef struct {
    int x, y;   /* - Coordinates - */

    int w, h;   /* - Dimensions - */
} Attributes;

/* --- Entity ---
 * Coordinate and size attributes. */
class Entity: public Coords
{
public:
    Entity();

    /* Access size attributes */
    int wth();
    int hgt();

    /* Set size attributes */
    void setSize(int w, int h);

    /* Set all of the members and attributes
     * of Entity by individual arguments */
    void setup(int x, int y, int w, int h);

    bool ptBndTest(int x, int y);

    void loadAttributes(Attributes);

protected:
    /* Size attributes
        width and height */
    int w, h;

    /* breach */
    bool breach;
};

#endif
