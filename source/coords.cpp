/*
 * =====================================================================================
 *
 *       Filename:  coords.cpp
 *        Project:  Breakpoint
 *
 *    Description:  Superclass coordinate declaration and management.
 *
 *         Author:  Kilgore
 *   Organization:  Kruft
 *
 * =====================================================================================
 */

/* Accessing Coordinates */
#include "coords.h"

/* -- Coords Constructor -- */
Coords::Coords() {
    x = y = 0;
}

/* -- Get X Coordinate -- */
int Coords::gtX() {
    return x;
}

/* -- Get Y Coordinate -- */
int Coords::gtY() {
    return y;
}

/* -- Set Coordinates -- */
void Coords::setCoords(int x, int y) {
    this->x = x;
    this->y = y;
}
