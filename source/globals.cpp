/*
 * =====================================================================================
 *
 *       Filename:  globals.cpp
 *        Project:  -TODO: Project / App Title-
 *
 *    Description:  global variables for -TODO-
 *
 *         Author:  -TODO: Authors-
 *   Organization:  -TODO: Organisation / Company-
 *
 * =====================================================================================
 */

// Using SDL
#include <SDL2/SDL.h>

/* Header File */
#include "globals.h"

/* Window being rendered to */
SDL_Window* gWindow = NULL;

/* Renderer for gWindow */
SDL_Renderer* gRenderer = NULL;
