/*
 * ==================================================================================
 *
 *       Filename:  screen.cpp
 *        Project:  Breakpoint
 *
 *    Description:  screen constants for program
 *
 *         Author:  Kilgore
 *   Organization:  Litus Kraft
 *
 * ==================================================================================
 */

/* Header file */
#include <SDL2/SDL.h>
#include "globals.h"
#include "screen.h"

/* Screen dimensions */
int scr_w = 340;
int scr_h = 510;

/* Screen Dimension Scale for HDPI displays */
int scr_s = scr_w;

/* Screen Frames Per Seconds */
const int SCREEN_FPS = 60;
const int SCREEN_TPF = 1000 / SCREEN_FPS;

/* -- Ratio Adjust --
 * Centres width/height 'a' at b/r,
 * where r defaults to 2 and b to
 * screen width. */

int ratio_adj(int object, int field, double div) {
    return ( (field/div) - (object / 2) );
}

/* -- Display Margin --
 * Displays a margin in dotted format */
void dispMargin(int y, int sections, int width, int height) {
    SDL_SetRenderDrawColor(gRenderer, 75, 99, 131, 0xFF);
    int space, edge_space;

    if (width < 1) {
        width = scr_w/((sections * 2) + 1);
    }
    if (height < 1)    {
        height = width;
    }

    space = (scr_w - (sections * width)) / (sections + 1);

    edge_space = (scr_w - ( sections * width + ((sections + 1) * space) ))/2;

    for (double i = 0; i < sections; i++) {
        SDL_Rect margin_line = {
            edge_space + space + ((width + space)*i), y - height/2, width, height
        };

        SDL_RenderFillRect(gRenderer, &margin_line);
    }
 }

/* -- Get Mouse Coordinates --
 * returns modified mouse coordinates,
 * with consideration of HDPi displays */

 void getMouseCoords(int &x, int &y) {
     SDL_GetMouseState(&x, &y);

     // Modify for HDPi screens
     x *= scr_s;
     y *= scr_s;
 }
