#ifndef COORDS_H
#define COORDS_H

#include <SDL2/SDL.h>

/*  --- Coordinates ---
    Superclass prototype for object coordinate
    declaration and management. */

class Coords
{
public:
    Coords();

    /* Access coordinate attributes */
    int gtX();
    int gtY();

    /* Set coordinate attributes */
    void setCoords(int, int);

protected:
    int x, y;
};

#endif
