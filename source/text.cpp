/*
 * ==================================================================================
 *
 *       Filename:  text.cpp
 *        Project:  Breakpoint
 *
 *    Description:  Holds global text attributes
 *
 *         Author:  Kilgore
 *   Organization:  Litus Kraft
 *
 * ==================================================================================
 */

#include <SDL2/SDL.h>
#include <SDL2_ttf/SDL_ttf.h>
#include <string>
#include <stdint.h>

#include "globals.h"
#include "text.h"
#include "colour.h"
#include "screen.h"

/* Fonts */
TTF_Font *title_font = NULL;            /* Title font */
TTF_Font *text_font = NULL;             /* Body text font */
TTF_Font *button_font = NULL;           /* Button title Font */

/* Creates and loads all the fonts for the project */
void loadFonts() {
    // Open fonts
    text_font = TTF_OpenFont("resources/Inconsolata.otf", scr_w/20);
    title_font = TTF_OpenFont("resources/lobster.ttf", scr_w/10);
    button_font = TTF_OpenFont("resources/lobster.ttf", scr_w/15);
}

/* Default Constructor
 * TODO: Other constructors  */
Text::Text() {
    tTexture = NULL;
    text = " ";
    desc = "";
    SDL_Color temp_color = {0, 0, 0};
    color = temp_color;
}

/* Destroy text class */
Text::~Text() {
    // Deallocate
    printf("Deleting texture titled %s %s\n", text.c_str(), desc.c_str());
    free();
}

/* Set displayed text and set text description */
void Text::setText(string text, string desc) {
    this->text = text;
    this->desc = desc;
}

/* Set decription of text. This is
 * used in discerning text objects
 * which have the same displayed text */ 
void Text::setDesc(string desc) {
    this->desc = desc;
}

/* Set transparency of text */
void Text::setAlpha(int alpha) {
    SDL_SetTextureAlphaMod(tTexture, alpha);
}

/* Get RGB color */
void Text::getRgb(int &r, int &g, int &b) {
    r = color.r;
    g = color.g;
    b = color.b;
}

/* Set color of text with SDL_Color */
void Text::setColor(SDL_Color color) {
    this->color = color;
}

/* Set color of text with RGB variables */
void Text::setRgbColor(int r, int g, int b) {
    SDL_Color temp_color = {r, g, b};
    color = temp_color;
}

/* Set font of text */
void Text::setFont(TTF_Font *font) {
	this->disp_font = font;
}

/* Deallocate texture */
void Text::free() {
    // Free texture if it exists
    if (tTexture != NULL) {
        SDL_DestroyTexture(tTexture);
        tTexture = NULL;
        w = h = 0;
    }
}

/* Updates texture of text with text_font specified */
bool Text::updateTexture() {
    // Deallocate preexisting texture
    free();

    // Render text surface
    SDL_Surface* tSurface = TTF_RenderText_Blended(disp_font, text.c_str(), color);

    if (tSurface == NULL) {
        printf("tSurface failed to be created! SDL_ttf Error: %s\n", TTF_GetError());
    } else {
        // Create text texture from surface pixels
        tTexture = SDL_CreateTextureFromSurface(gRenderer, tSurface);

        if (tTexture == NULL) {
            printf( " Texture couldn't be created from surface! SDL_ttf Error: %s\n",
					TTF_GetError() );
        } else {
            // Set size attributes
            SDL_QueryTexture(tTexture, NULL, NULL, &w, &h);
        }

        // Deallocate surface
        SDL_FreeSurface(tSurface);
        tSurface = NULL;
    }

    return tTexture != NULL;
}

/* Sets up the core attributes of text class and processes a texture for text */
void Text::setup(string text, SDL_Color color, TTF_Font *font) {
    setText(text);
    setColor(color);
	setFont(font);	
    updateTexture();
}

void Text::render() {
    //Set rendering space
	SDL_Rect renderQuad = {x, y, w, h};

	//Render to screen
	SDL_RenderCopy(gRenderer, tTexture, NULL, &renderQuad);
}
