#ifndef COLOUR_H
#define COLOUR_H

/* - Decoration Colors - */
extern SDL_Color bg_color;

/* - Text Colors - */
extern SDL_Color title_color;
extern SDL_Color body_color;

/* - General Color - */
extern SDL_Color misc_color;

/* - General Colors - */
extern SDL_Color grey;
extern SDL_Color black;

#endif
