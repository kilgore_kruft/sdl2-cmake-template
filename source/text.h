#ifndef TEXT_H
#define TEXT_H

#include <SDL2/SDL.h>
#include <SDL2_ttf/SDL_ttf.h>
#include <string>
using namespace std;

#include "entity.h"
#include "colour.h"

/* Fonts */
extern TTF_Font *title_font;           /* Title Font */
extern TTF_Font *text_font;            /* Body text Font */
extern TTF_Font *button_font;          /* Button title Font */

// Opens font files as TTF_Font	
void loadFonts();

/* Object holding functions to help display text */
class Text: public Entity
{
public: 
    Text(); 
    ~Text();	

    
    bool updateTexture(); 
    void render(); 
    void setText(string , string = "");
	void setFont(TTF_Font *);
    void setDesc(string );
    void setAlpha(int );
    void getRgb(int &, int &, int &);
    void setColor(SDL_Color );
    void setRgbColor(int , int , int );	
    void setup(string , SDL_Color  = body_color, TTF_Font* = text_font); 
    void free();

private:
    SDL_Texture* tTexture;  /* Stores text to be displayed as texture */
    string text, desc;
    SDL_Color color;
	TTF_Font *disp_font;
};

#endif
