#ifndef FUNCTIONS_H
#define FUNCTIONS_H

/* Initialises SDL and creates window */
bool init();

/* Load resources */
bool loadMedia();

/* Frees media and shuts down SDL */
void end();

#endif
