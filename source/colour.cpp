#include <SDL2/SDL.h>

/* Colors
 * https://color.adobe.com/Blue-color-theme-6177531/
 * https://color.adobe.com/theme-1-color-theme-6170784/
 * https://color.adobe.com/Theme-4-color-theme-6179604/
 */

/* - Background Color - */
SDL_Color bg_color = {37, 60, 89};

/* - Title Text Color - */
SDL_Color title_color = {228, 224, 218};

/* - Body Text Color - */
SDL_Color body_color = {132, 153, 177};

/* - Miscellaneous Color - */
SDL_Color misc_color = {91, 90, 140};

/* - General Colors - */
SDL_Color grey = {29, 29, 29};
SDL_Color black = {0, 0, 0};
