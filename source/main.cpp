/*
 * ==================================================================================
 *
 *       Filename:  main.cpp
 *        Project:  -TODO: Project / App Title-
 *
 *    Description:  -TODO-
 *
 *         Author:  -TODO: Authors-
 *   Organization:  -TODO: Organisation / Company-
 *
 * ==================================================================================
 */

// Using SDL2, SDL2_ttf and string libs
#include <SDL2/SDL.h>
#include "SDL2_ttf/SDL_ttf.h"
#include <string.h>

// Accessing global and screen variables,
// as well as SDL functions.
#include "screen.h"
#include "globals.h"
#include "sdl_functions.h"
#include "text.h"
#include "colour.h"

const int FRAMES_PER_SECOND = 60;

int main(int argc, char* args[]) {
    // Starts up SDL and creates window
    if (!init()) {
        printf("SDL failed to initialise.\n");
    } else if (!loadMedia()) {
        printf("Media could not be loaded.\n");
    } else {
        bool running = true;    /* Flags program exit in main loop */
        SDL_Event e;            /* Event handler */

		// FIXME: Delete this text!	
		Text text;
		text.setup("Template Project!", title_color, title_font);
		text.setCoords(ratio_adj(text.wth()), ratio_adj(text.hgt(), scr_h));
		//

		// Main loop
		while (running) {
            // Event Queue handling
            while (SDL_PollEvent(&e) != 0) {
                // NOTE: SDL_Keycode e = event.key.keysym.sym;
                // User requests quit
                if (e.type == SDL_QUIT) {
                    running = false;
                }
            }	

            // Clear renderer w/ Black Pearl
            SDL_SetRenderDrawColor(gRenderer, 37, 60, 89, 0xFF);
            SDL_RenderClear(gRenderer);

			text.render();	// FIXME: Delete this line!

            // Update screen
            SDL_RenderPresent(gRenderer); 
        }
    }

    // Exit Program
    end();

    return 0;
}
