CFLAGS = -Wall

default: unix_run

# Clean Target
# Removes the build files and latest binary
clean:
	rm -Rf ./build
	rm -Rf ./bin

# Dirs Target
# Removes the build files, if they exist, then recreates them
dir:
	rm -Rf ./build
	mkdir build
	cp ./makefile_build ./build
	mv ./build/makefile_build ./build/makefile
	rm -Rf ./bin
	mkdir bin
	cp -R ./resources ./bin/resources

# Run Target
# runs the executable in /bin
open:
	cd bin && ./source && cd ..

# unix target
# Makes unix by copying makefiles in directory "build",
# and running makefiles in that directory
unix:
	make dir && cd build && make unix

# unix_run target
# The same as the unix target, but also runs the executable in /bin
unix_run:
	make dir && cd build && make unix_run

# Clean out Git
rid_of_git:
	rm -Rf .git
