# SDL2 CMake Template

## What is this repository for?
This is version 1.0 of a template project for SDL2 development, using CMake. It's a personal project mostly, but freely available.

## How do I get set up?
1. Clone repository.
2. Type in terminal the following to generate binary (only tested on mac for now): `` make unix ``
3. Create your own SDL2 projects!

### Dependencies
* You can get SDL 2 from [here](https://www.libsdl.org/), with installation instructions [here](https://wiki.libsdl.org/Installation).
* You will also need CMake 3.0 from [here](http://www.cmake.org/)
* they can both be installed with homebrew and apt-get (read on).

### Installing Dependencies with Package Managers (Unix)
Mac installations instructions use [homebrew](http://brew.sh/). This can be downloaded from terminal with the following command:
~~~
ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
~~~

### CMake
* **Mac**: ``brew install cmake``
* **Linux**: Unfortunately CMake 3.0 is not currently available through apt-get. A cmake 3.2.2 tar can be downloaded from [here](http://goo.gl/RYM2BN).  
Make sure you have a c++ compiler: ``sudo apt-get install g++.``

### SDL2
* **Mac**: ``brew install sdl2``
* **Linux**: ``sudo apt-get install libsdl2-2.0-0``

## Directories

**_/bin_** binary folder
**_/build_** makefiles for binary.
**_/source_** source files.
**_/res_** copy of makefile (temporary)
**_/cmake_** SDL2 cmake files, for finding SDL2 framework.

## Credits

* The CMakeLists.txt is largely based on [Twinklebear's](http://www.willusher.io/pages/sdl2/) tutorials.
* [Lazyfoo](http://www.willusher.io/pages/sdl2/) SDL2 tutorials were used for a lot of the foundational code
